# pipeline template

<!-- BADGIE TIME -->

<!-- END BADGIE TIME -->

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

Template for CI/CD pipeline projects. One way to use this is to clone this repo locally, delete the .git directory, rename, run `git init`, and push to empty project in GitLab. Otherwise, a project can be created from this template in GitLab, the repo history will contain the the history of this template project.

Any pipeline project should:
- Be public
- No deployment target
- Name is lower-case snake-case
- Disable unused GitLab features. Disable Git LFS.
- Have a project description
- protect tags with wildcard `*` so only maintainers can create tags.
- Default branch is main
- Use cici-tools


Edit README with appropriate information.



- [Project setup](#project-setup)

- [Use cases](#use-cases)

- [Targets](#targets)

- [Variables](#variables)

## Project setup

At a minimum, this pipeline requires that:

- List files that are required.

- List directories that are required.

## Targets


## Use cases

### All includes

Add the following to your `.gitlab-ci.yml` file to enable all targets:

```yaml
include:
  - project: buildgarden/pipelines/pipeline
    file:
      - pipeline-job.yml
```

### All hooks

Add the following to your `.pre-commit-config.yaml` file to enable all targets:

```yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/pipeline
    hooks:
      - id: pipeline-job
```

## Variables

The following variables are required:

### `PIPELINE_VARIABLE`

Variable description.

Example:

```yaml
PIPELINE_VARIABLE: pipeline
```
